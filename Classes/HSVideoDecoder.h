//
//  HSVideoDecoder.h
//  LearnOpenGLES
//
//  Created by Macrovideo on 2018/4/14.
//  Copyright © 2018年 林伟池. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HSFrameData.h"

 
@interface HSVideoDecoder : NSObject


//初始化解码的参数
-(BOOL)initDecoder:(int)nType param:(id)param;
//返回值为解码数据大小
- (int)decodeData:(PHSFRAME_DATA)inFrameData  output:(PHSFRAME_DATA)outFrameData;//add by luo 20180816
-(void)releaseDecoder;
@end
