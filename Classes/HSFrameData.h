//
//  HSFrameData.h
//
//  Created by Macrovideo on 2018/10/22.
//


#ifndef HS_FRAME_DATA
#define HS_FRAME_DATA
typedef struct tagFrameData{
    int nType;
    int nSize;
    char *data;
}HSFRAME_DATA, *PHSFRAME_DATA;


#endif

